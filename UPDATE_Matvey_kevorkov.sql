-- Step 1: Update the rental duration and rental rates for the film
UPDATE film
SET rental_duration = 21,
    rental_rate = 9.99,
    last_update = NOW()
WHERE title = 'Poor Things';

-- Step 2: Identify a customer with at least 10 rentals and 10 payments and update their personal data
WITH CustomerToUpdate AS (
    SELECT c.customer_id
    FROM customer c
    JOIN rental r ON c.customer_id = r.customer_id
    JOIN payment p ON c.customer_id = p.customer_id
    GROUP BY c.customer_id
    HAVING COUNT(r.rental_id) >= 10 AND COUNT(p.payment_id) >= 10
    LIMIT 1
)
UPDATE customer
SET first_name = 'Matvey',
    last_name = 'Kevorkov',
    email = 'myemail@example.com',
    address_id = (SELECT address_id FROM address LIMIT 1),
    create_date = CURRENT_DATE,
    last_update = NOW()
WHERE customer_id = (SELECT customer_id FROM CustomerToUpdate);
